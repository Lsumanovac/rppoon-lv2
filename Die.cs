﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace RPPOON_LV2
{
    class Die
    {
        private int numberOfSides;
        //private Random randomGenerator; // 1. i 2. zadatak
        private RandomGenerator randomGenerator;
        public Die(int numberOfSides)
        {
            this.numberOfSides = numberOfSides;
            //this.randomGenerator = new Random(); //1 zadatak
            this.randomGenerator = RandomGenerator.GetInstance();
        }

        //public Die(int numberOfSides, Random random) // 2. zadatak
        //{
        //    this.numberOfSides = numberOfSides;
        //    this.randomGenerator = random;
        //}

        public int Roll()
        {
            //int rolledNumber = randomGenerator.Next(1, numberOfSides + 1); // 1. i 2. zadatak
            int rolledNumber = this.randomGenerator.NextInt(1, numberOfSides + 1);
            return rolledNumber;
        }
    }
}
