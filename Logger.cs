﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace RPPOON_LV2
{
    class Logger
    {
        private string type;
        private string filePath;

        public Logger(string type, string filePath)
        {
            this.type = type;
            this.filePath = filePath;
        }

        public void Log(string messege)
        {
            if (this.type.Equals("Console"))
                Console.WriteLine(messege);
            else if (this.type.Equals("File"))
                using (System.IO.StreamWriter writer =
                    new System.IO.StreamWriter(this.filePath))
                {
                    writer.WriteLine(messege);
                }
            else
                throw new ArgumentException("Unknown type");
        }
    }
}
