using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace RPPOON_LV2
{
    class Program
    {
        static void Main(string[] args)
        {
            DiceRoller diceRoller = new DiceRoller();
            int i;
            Random random = new Random();

            for(i=0; i<20; i++)
            {
                diceRoller.InsertDie(new Die(6)); //1. zadatak i 3. zadatak, 4. zadatak
                //diceRoller.InsertDie(new Die(6, random)); // 2. zadatak
            }

            diceRoller.RollAllDice();
            IList<int> rollingResults = diceRoller.GetRollingResults();
            foreach(int result in rollingResults)
            {
                Console.WriteLine(result);
            }
        }
    }
}
